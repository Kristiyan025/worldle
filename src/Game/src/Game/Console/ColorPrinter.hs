module Game.Console.ColorPrinter (colorizeText, colorizeTextFromEnumColor, colorSequence, Color(..)) where

import qualified Data.Map as Map

import Game.GameColors (gray, yellow, green, invalid)


-- ANSI escape codes for text color
data Color = Red | Green | Yellow | Blue | Magenta | Cyan | Reset | LightGray

colorizeText :: Char -> String -> String
colorizeText color text =
  case Map.lookup color colorMap of
    Nothing -> error $ "Invalid color: " ++ [color]
    Just color -> colorizeTextFromEnumColor color text

-- Function to print colored text
colorizeTextFromEnumColor :: Color -> String -> String
colorizeTextFromEnumColor color text = (colorSequence color) ++ text ++ "\x1b[0m"

colorSequence :: Color -> String
colorSequence color = "\x1b[" ++ code ++ "m"
  where
    code = case color of
      Red -> "31"
      Green -> "32"
      Yellow -> "33"
      Blue -> "34"
      Magenta -> "35"
      Cyan -> "36"
      LightGray -> "37"
      Reset -> "0"

colorMap :: Map.Map Char Color
colorMap = Map.fromList
  [ (gray, LightGray)
  , (yellow, Yellow)
  , (green, Green)
  , (invalid, Red)
  ]
