module Game.Console.ConsoleMode (ConsoleMode(..), getConsolePrefix, printStr, printLine) where

import qualified Data.Map as Map


data ConsoleMode = MainConsole | SubConsole deriving (Show, Eq, Ord)

consolePrefixMap :: Map.Map ConsoleMode String
consolePrefixMap = Map.fromList
  [ (MainConsole, " > ")
  , (SubConsole, "~~> ")
  ]

getConsolePrefix :: ConsoleMode -> String
getConsolePrefix mode = case Map.lookup mode consolePrefixMap of
  Just prefix -> prefix
  Nothing -> error "Invalid console mode."


printStr :: ConsoleMode -> String -> IO ()
printStr mode line = putStr $ (getConsolePrefix mode) ++ line

printLine :: ConsoleMode -> String -> IO ()
printLine mode line = putStrLn $ (getConsolePrefix mode) ++ line