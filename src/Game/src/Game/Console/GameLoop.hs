{-# LANGUAGE BlockArguments #-}
module Game.Console.GameLoop (gameLoop, GameLoop) where

import System.IO (stdout, hFlush)

import Game.Console.GameLoopConfig (GameLoopConfig(..), printStr, printLine)
import Game.Console.InputReader (readLine)
import Game.Console.ColorPrinter (colorizeTextFromEnumColor, colorSequence, Color(..))


type GameLoop a b = a -> GameLoopConfig a b -> IO Bool

gameLoop :: GameLoop a b
gameLoop currentState gc = do
  (inputLine, inputState) <-
    if ((shouldAwaitInput gc) currentState) then do
      -- Print message to the user
      message <- (messageFactory gc) currentState
      (printStr gc) $ (colorizeTextFromEnumColor Magenta message) ++ colorSequence Cyan
      -- Flush the standard output buffer, because we're not printing a newline
      hFlush stdout

      -- Get input from the user
      inputLine <- readLine ()
      inputState <- (getInputState gc) inputLine currentState

      -- Clear the console color
      putStr $ colorSequence Reset
      hFlush stdout

      return (inputLine, inputState)
    else do
      inputState <- (getInputState gc) "" currentState
      return ("", inputState)

  -- Handle input
  if ((isExitInputState gc) inputState) then do
    (printLine gc) $ colorizeTextFromEnumColor Blue $ (exitMessage gc)
    return False
  else if not ((isValidInputState gc) inputState) then do
    invalidInputMessage <- (invalidInputMessageFactory gc) inputLine inputState currentState
    (printLine gc) $ colorizeTextFromEnumColor Red $ invalidInputMessage
    gameLoop currentState gc
  else
    updateStateAndLoop inputLine
  where
    updateStateAndLoop :: String -> IO Bool
    updateStateAndLoop inputLine = do
      nextState <- (modifyState gc) currentState inputLine
      if (isSuccessState gc) nextState then do
        successMessage <- (successMessageFactory gc) nextState
        (printLine gc) $ colorizeTextFromEnumColor Green $ successMessage
        return True
      else if (isFailureState gc) nextState then do
        failureMessage <- (failureMessageFactory gc) nextState
        (printLine gc) $ colorizeTextFromEnumColor Red $ failureMessage
        return False
      else
        gameLoop nextState gc
