module Game.Console.InputReader (readLine) where

import Data.Text (unpack, pack, strip)
import Data.Char (toLower)


toLowerAndTrim :: String -> String
toLowerAndTrim = unpack . strip . pack . map toLower

readLine :: () -> IO String
readLine _ = do
    input <- getLine
    let trimmedAndLowercasedLine = toLowerAndTrim input
    return trimmedAndLowercasedLine
