module Game.Console.GameStatePrinter (printGameState) where

import Data.List (intercalate)
import Data.Char (toUpper)

import Game.Typing (MatchedGuesses)
import Game.Modes.Play.GameState (GameState(..))
import qualified Game.Console.ConsoleMode as CM (ConsoleMode(..), printLine)
import Game.Console.ColorPrinter (colorizeText)


printGameState :: Int -> MatchedGuesses -> CM.ConsoleMode -> IO ()
printGameState wordLength guessesAndMatches mode = do
  printLine separatorLine

  _ <- mapM (\(guess, match) -> do
        printLine $ formatGuess guess match
        printLine separatorLine)
      $ reverse guessesAndMatches

  return ()
  where
  printLine :: String -> IO ()
  printLine line = CM.printLine mode line

  separatorLine :: String
  separatorLine = "+" ++ (intercalate "" (replicate wordLength "---+"))

  formatGuess :: String -> String -> String
  formatGuess guess match = "|" ++ (intercalate ""
        (map (\(letter, color) -> " " ++ (colorizeText color $ [toUpper letter])
        ++ " |") $ zip guess match))
