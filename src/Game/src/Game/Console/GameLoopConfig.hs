module Game.Console.GameLoopConfig (GameLoopConfig(..), consolePrefix, printStr, printLine) where

import qualified Game.Console.ConsoleMode as CM (ConsoleMode(..), getConsolePrefix, printStr, printLine)


data GameLoopConfig a b = GameLoopConfig
  { modifyState :: a -> String -> IO a
  , isSuccessState :: a -> Bool
  , isFailureState :: a -> Bool
  , shouldAwaitInput :: a -> Bool
  , messageFactory :: a -> IO String
  , getInputState :: String -> a -> IO b
  , isValidInputState :: b -> Bool
  , isExitInputState :: b -> Bool
  , exitMessage :: String
  , invalidInputMessageFactory :: String -> b -> a -> IO String
  , successMessageFactory :: a -> IO String
  , failureMessageFactory :: a -> IO String
  , consoleMode :: CM.ConsoleMode
  }

consolePrefix :: GameLoopConfig a b -> String
consolePrefix gameLoopConfig = CM.getConsolePrefix $ consoleMode gameLoopConfig

printStr :: GameLoopConfig a b -> String -> IO ()
printStr gameLoopConfig line = CM.printStr (consoleMode gameLoopConfig) line

printLine :: GameLoopConfig a b -> String -> IO ()
printLine gameLoopConfig line = CM.printLine (consoleMode gameLoopConfig) line
