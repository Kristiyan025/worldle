module Game.StringUtils (formatOrdinalNumber, isLowerEnglishLettersOnly, splitAndFilter) where

import Data.Char (isLower)

formatOrdinalNumber :: Int -> String
formatOrdinalNumber number = case number of
  1 -> "1st"
  2 -> "2nd"
  3 -> "3rd"
  _ -> (show number) ++ "th"

isLowerEnglishLettersOnly :: String -> Bool
isLowerEnglishLettersOnly = all isLower

splitAndFilter :: String -> [String]
splitAndFilter line = filter (not . null) $ words line
