{-# LANGUAGE UnicodeSyntax #-}
module Game.GameColors (gray, yellow, green, invalid) where


gray :: Char
gray = '⬜'

yellow :: Char
yellow = '🟨'

green :: Char
green = '🟩'

invalid :: Char
invalid = '❌'
