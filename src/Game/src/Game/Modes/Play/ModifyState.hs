{-# LANGUAGE BlockArguments #-}
module Game.Modes.Play.ModifyState (modifyState) where

import System.Random (randomRIO)
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.List (intersect)

import qualified Game.Modes.Play.GameState as GS
import Game.Modes.Play.GameState (GameState(..))
import Game.Modes.Play.Difficulty (Difficulty(..))
import Game.Modes.Play.ConsoleMode (printLine)
import Game.WordMatcher (matchWord, makeLetterDistribution, getLetterCount, LetterDistribution)
import Game.GameColors (gray, yellow, green, invalid)
import Game.Console.ColorPrinter (colorizeTextFromEnumColor, Color(..))


modifyState :: GameState -> String -> IO GameState
modifyState state input = do
  newState <- modifyStateWithoutValidations state input
  validatedState <- validateState newState state input
  return validatedState

modifyStateWithoutValidations :: GameState -> String -> IO GameState
modifyStateWithoutValidations state guessWord = do
  let match = matchWord guessWord $ secretWord state :: String

  modifyStateWithoutValidationsWithMatch state guessWord match $ hasLiedAlready state

modifyStateWithoutValidationsWithMatch :: GameState -> String -> String -> Bool -> IO GameState
modifyStateWithoutValidationsWithMatch state guessWord match hasLiedAlready = do
  return GameState
    { difficulty = difficulty state
    , secretWord = secretWord state
    , guesses = (guessWord:(guesses state))
    , matches = (match:(matches state))
    , maxGuesses = maxGuesses state
    , GS.words = GS.words state
    , foundGrays = updateFoundGrays (foundGrays state) match guessWord
    , foundYellows = updateFoundYellows (foundYellows state) match guessWord
    , foundGreens = updateFoundGreens (foundGreens state) match guessWord
    , hasLiedAlready = hasLiedAlready
    , lieProbability = lieProbability state
    }

updateFoundGrays :: String -> String -> String -> [Char]
updateFoundGrays foundGrays match guessWord = foundGrays ++ (updateFoundGraysHelper foundGrays match guessWord)
  where
    updateFoundGraysHelper :: String -> String -> String -> [Char]
    updateFoundGraysHelper _ [] [] = foundGrays
    updateFoundGraysHelper foundGrays (match:ms) (letter:ls) = let
      restOfFoundGrays = updateFoundGraysHelper foundGrays ms ls :: [Char]
      in if match == gray
            && notElem letter foundGrays
            && notElem letter restOfFoundGrays
        then [letter] ++ restOfFoundGrays
        else restOfFoundGrays

updateFoundGreens :: String -> String -> String -> String
updateFoundGreens foundGreens match guessWord = updateFoundGreensHelper foundGreens match guessWord
  where
    updateFoundGreensHelper :: String -> String -> String -> String
    updateFoundGreensHelper _ [] [] = []
    updateFoundGreensHelper (foundGreen:fs) (match:ms) (letter:ls) = let
      restOfFoundGreens = updateFoundGreensHelper fs ms ls :: String
      in if match == green
        then [letter] ++ restOfFoundGreens
        else [foundGreen] ++ restOfFoundGreens

updateFoundYellows :: LetterDistribution -> String -> String -> LetterDistribution
updateFoundYellows foundYellows match guessWord = let
  uniqueLetters = Set.toList $ Set.fromList $ guessWord :: [Char]
  in updateYellowDistribution foundYellows uniqueLetters match guessWord
  where
    updateYellowDistribution :: LetterDistribution -> [Char] -> String -> String -> LetterDistribution
    updateYellowDistribution yellowDistribution [] _ _ = yellowDistribution
    updateYellowDistribution yellowDistribution (letter:ls) match guessWord  = let
      currentLetterCount = countColorsForLetter letter [yellow, green] match guessWord :: Int
      restOfYellowDistribution = updateYellowDistribution yellowDistribution ls match guessWord :: LetterDistribution
      in if currentLetterCount > (getLetterCount letter restOfYellowDistribution)
        then Map.insert letter currentLetterCount restOfYellowDistribution
        else restOfYellowDistribution

countColorForLetter :: Char -> Char -> String -> String -> Int
countColorForLetter _ _ [] [] = 0
countColorForLetter letter color (match:ms) (wordLetter:ws) = let
  restOfYellows = countColorForLetter color letter ms ws :: Int
  in if letter == wordLetter && match == color
    then 1 + restOfYellows
    else restOfYellows

countColorsForLetter :: Char -> [Char] -> String -> String -> Int
countColorsForLetter _ [] _ _ = 0
countColorsForLetter letter (color:cs) match word = let
  currentColorCount :: Int
  currentColorCount = countColorForLetter letter color match word
  in currentColorCount + (countColorsForLetter letter cs match word)

validateInDatabase :: GameState -> GameState -> String -> IO GameState
validateInDatabase newState oldState guessWord = do
  if notElem guessWord (GS.words oldState)
    then do
    printLine $ colorizeTextFromEnumColor Yellow "WARNING: Word not in database"
    return oldState
  else return newState

validateGrays :: GameState -> GameState -> String -> IO GameState
validateGrays newState oldState guessWord = do
  let match = matchWord guessWord $ secretWord oldState :: String
  let freeLetters = map fst $ filter (\(_, match) -> match == gray) $ zip guessWord match
  if haveIntersection (foundGrays oldState) freeLetters
    then do
    printLine $ colorizeTextFromEnumColor Yellow "WARNING: Word includes already found gray letters"
    return oldState
  else return newState

haveIntersection :: Eq a => [a] -> [a] -> Bool
haveIntersection list1 list2 = not $ null (list1 `intersect` list2)

validateGreens :: GameState -> GameState -> String -> IO GameState
validateGreens newState oldState guessWord = do
  if validateGreensHelper (foundGreens oldState) (secretWord oldState) guessWord
    then do
    printLine $ colorizeTextFromEnumColor Yellow "WARNING: Word does not include already found green letters"
    return oldState
  else return newState
  where
    validateGreensHelper :: String -> String -> String -> Bool
    validateGreensHelper [] [] [] = False
    validateGreensHelper (foundGreen:fs) (secretWordLetter:ss) (wordLetter:ws)  =
      (foundGreen == green && secretWordLetter /= wordLetter) || (validateGreensHelper fs ss ws)

validateYellows :: GameState -> GameState -> String -> IO GameState
validateYellows newState oldState guessWord = do
  let
      uniqueSecretWordLetters :: [Char]
      uniqueSecretWordLetters = Set.toList $ Set.fromList $ secretWord oldState

      greenDistribution :: LetterDistribution
      greenDistribution = buildGreensDistribution uniqueSecretWordLetters

      guessWordDistribution :: LetterDistribution
      guessWordDistribution = makeLetterDistribution guessWord

      yellowDistribution :: LetterDistribution
      yellowDistribution = foundYellows oldState

      uniqueLetters :: [Char]
      uniqueLetters = Set.toList $ Set.fromList $ (Map.keys greenDistribution) ++ (Map.keys guessWordDistribution)
        ++ (Map.keys yellowDistribution)
  if validateYellowsHelper uniqueLetters yellowDistribution greenDistribution guessWordDistribution
    then do
      printLine $ colorizeTextFromEnumColor Yellow "WARNING: Word does not include already found yellow letters"
      return oldState
    else return newState
  where
    buildGreensDistribution :: [Char] -> LetterDistribution
    buildGreensDistribution [] = Map.empty
    buildGreensDistribution (letter:ls)  = let
      currentLetterCount :: Int
      currentLetterCount = countColorForLetter letter green (foundGreens oldState) (secretWord oldState)

      restOfGreenDistribution :: LetterDistribution
      restOfGreenDistribution = buildGreensDistribution ls
      in Map.insert letter currentLetterCount restOfGreenDistribution

    validateYellowsHelper :: [Char] -> LetterDistribution -> LetterDistribution -> LetterDistribution -> Bool
    validateYellowsHelper [] _ _ _ = False
    validateYellowsHelper (letter:ls) yellowDistribution greenDistribution guessDistribution = let
      yellowCount = getLetterCount letter yellowDistribution :: Int
      greenCount = getLetterCount letter greenDistribution :: Int
      guessCount = getLetterCount letter guessDistribution :: Int
      in (yellowCount - greenCount > guessCount)
        || (validateYellowsHelper ls yellowDistribution greenDistribution guessDistribution)

validateState :: GameState -> GameState -> String -> IO GameState
validateState newState oldState guessWord = do
  if (difficulty oldState) == Easy
    then do
      dbValidatedState <- validateInDatabase newState oldState guessWord
      graysValidatedState <- validateGrays dbValidatedState oldState guessWord
      greensValidatedState <- validateGreens graysValidatedState oldState guessWord
      yellowsValidatedState <- validateYellows greensValidatedState oldState guessWord

      return yellowsValidatedState
    else return newState

decideWhetherToLie :: GameState -> GameState -> String -> IO GameState
decideWhetherToLie newState oldState guessWord = do
  if (difficulty oldState) == Expert && not (hasLiedAlready oldState)
    then do
      let lieProbabilityThreshold = lieProbability oldState :: Double
      lieOutcome <- randomRIO (0.0, 1.0)
      if lieOutcome <= lieProbabilityThreshold
        then do
          match <- createBelievableLieMatch oldState guessWord
          imposterState <- modifyStateWithoutValidationsWithMatch oldState guessWord match True
          return imposterState
      else return newState
    else return newState

createBelievableLieMatch :: GameState -> String -> IO String
createBelievableLieMatch state guessWord = do
  let lieSecretWord = checkDb (GS.words state) :: String
  if lieSecretWord == ""
  then do
    let actualMatch = matchWord guessWord (secretWord state) :: String
    return actualMatch
  else do
    let lieMatch = matchWord guessWord lieSecretWord :: String
    return lieMatch
  where
  checkForSpecificWord :: String -> Bool
  checkForSpecificWord pseudoSecretWord = all
    (\(guess, match) -> (matchWord guess pseudoSecretWord == match))
    $ zip (guesses state) (matches state)

  checkDb :: [String] -> String
  checkDb [] = ""
  checkDb (pseudoSecretWord:ps) =
    if pseudoSecretWord == (secretWord state)
    then checkDb ps
    else if checkForSpecificWord pseudoSecretWord
    then pseudoSecretWord
    else checkDb ps
