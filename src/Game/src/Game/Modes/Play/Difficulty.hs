module Game.Modes.Play.Difficulty (Difficulty(..), readDifficulty, difficultyOptions) where


import qualified Data.Map as Map

data Difficulty = Easy | Standard | Expert
  deriving (Show, Eq)

difficultyOptions :: [String]
difficultyOptions = ["easy", "standard", "expert"]

mapDifficulty :: Map.Map String Difficulty
mapDifficulty = Map.fromList [("easy", Easy), ("standard", Standard), ("expert", Expert)]

readDifficulty :: String -> Difficulty
readDifficulty s = case Map.lookup s mapDifficulty of
  Just d -> d
  Nothing -> error "Invalid difficulty"