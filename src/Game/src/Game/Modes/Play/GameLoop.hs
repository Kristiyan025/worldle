module Game.Modes.Play.GameLoop (playGameLoop, Difficulty(..)) where

import System.Random (randomRIO)
import qualified Data.Map as Map


import Game.Console.GameLoop (gameLoop)
import qualified Game.Console.GameLoopConfig as GLC (GameLoopConfig(..))
import qualified Game.Modes.Play.GameState as GS
import Game.Modes.Play.GameState (GameState(..), lastGuess, lastMatch, currentGuesses)
import Game.Modes.Play.ModifyState (modifyState)
import Game.Modes.Play.Difficulty (Difficulty(..))
import Game.Modes.Play.ConsoleMode (consoleMode)
import Game.Modes.Play.InputState (InputState(..))
import Game.Modes.ExitKeywords (exitWord, exitMessage)
import Game.GameColors (invalid)
import qualified Game.Console.GameStatePrinter as GSP (printGameState)
import Game.Console.ColorPrinter (colorizeTextFromEnumColor, Color(..))
import Database.Reader (readMainWordSetWithLength)
import Game.StringUtils (formatOrdinalNumber, isLowerEnglishLettersOnly)


playGameLoop :: Int -> Difficulty -> IO Bool
playGameLoop wordLength difficulty = do
  initialState <- initialStateFactory wordLength difficulty
  let
    gameLoopConfig :: GLC.GameLoopConfig GameState InputState
    gameLoopConfig = GLC.GameLoopConfig
      { GLC.modifyState = modifyState
      , GLC.isSuccessState = isSuccessState
      , GLC.isFailureState = isFailureState
      , GLC.shouldAwaitInput = const True
      , GLC.messageFactory = messageFactory
      , GLC.getInputState = getInputState
      , GLC.isValidInputState = isValidInputState
      , GLC.isExitInputState = isExitInputState
      , GLC.exitMessage = exitMessage
      , GLC.invalidInputMessageFactory = invalidInputMessageFactory
      , GLC.successMessageFactory = successMessageFactory
      , GLC.failureMessageFactory = failureMessageFactory
      , GLC.consoleMode = consoleMode
      }

  gameLoop initialState gameLoopConfig

initialStateFactory :: Int -> Difficulty -> IO GameState
initialStateFactory wordLength difficulty = do
  wordsDB <- readMainWordSetWithLength wordLength
  let maxGuesses = calculateMaxGuesses wordLength difficulty :: Int
  randomIndex  <- randomRIO (0, (length wordsDB) - 1)

  return GameState
    { GS.difficulty = difficulty
    , GS.secretWord = wordsDB !! randomIndex
    , GS.guesses = []
    , GS.matches = []
    , GS.maxGuesses = maxGuesses
    , GS.words = wordsDB
    , GS.foundGrays = []
    , GS.foundYellows = Map.empty
    , GS.foundGreens = replicate wordLength invalid
    , GS.hasLiedAlready = False
    , GS.lieProbability = 1.0 / (fromIntegral maxGuesses)
    }

isSuccessState :: GameState -> Bool
isSuccessState state = if null $ guesses state then False else lastGuess state == secretWord state

isFailureState :: GameState -> Bool
isFailureState state = currentGuesses state == maxGuesses state

messageFactory :: GameState -> IO String
messageFactory state = do
  (if (currentGuesses state) == 0
  then return ()
  else printGameState state)
  return $ "Give your " ++ (show $ formatOrdinalNumber $ (currentGuesses state) + 1) ++ " guess: "

getInputState :: String -> GameState -> IO InputState
getInputState input state =
  if input == exitWord
    then return Exit
  else if (length input) /= (length $ secretWord state)
    then return InvalidLength
  else if not $ isLowerEnglishLettersOnly input
    then return InvalidCharacters
  else return Valid

isValidInputState :: InputState -> Bool
isValidInputState = (==Valid)

isExitInputState :: InputState -> Bool
isExitInputState = (==Exit)

invalidInputMessageFactory :: String -> InputState -> GameState -> IO String
invalidInputMessageFactory _ inputState state = do
  case inputState of
    InvalidLength ->
      return $ "Invalid input: Word length does not match secret word length: "
        ++ (show $ length $ secretWord state)
    InvalidCharacters ->
      return "Invalid input: Word contains non-English symbols"
    _ -> error $ "Cannot create invalid input message for input state: " ++ (show inputState)

successMessageFactory :: GameState -> IO String
successMessageFactory state = do
  printGameState state
  return $ "You won! The secret word was indeed: " ++ (colorizeTextFromEnumColor Cyan $ secretWord state)
    ++ (colorizeTextFromEnumColor Green $ ". You guessed it in " ++ (show $ currentGuesses state) ++ " tries.")

failureMessageFactory :: GameState -> IO String
failureMessageFactory state = do
  return $ "You lost! The secret word was: " ++ (colorizeTextFromEnumColor Cyan $ secretWord state)
    ++ (colorizeTextFromEnumColor Red
      $ ". You couldn't guess it in " ++ (show $ maxGuesses state) ++ " guesses. Better luck next time!")

calculateMaxGuesses :: Int -> Difficulty -> Int
calculateMaxGuesses wordLength difficulty = case difficulty of
  Easy -> 2 * wordLength
  Standard -> wordLength + 2
  Expert -> ceiling $ 1.5 * (fromIntegral wordLength)

printGameState :: GameState -> IO ()
printGameState state = GSP.printGameState
  (length $ secretWord state)
  (zip (guesses state) (matches state))
  consoleMode
