module Game.Modes.Play.GameState (GameState(..), lastGuess, lastMatch, currentGuesses) where

import Game.WordMatcher (LetterDistribution)
import Game.Modes.Play.Difficulty (Difficulty(..))


data GameState = GameState
  { difficulty :: Difficulty
  , secretWord :: String
  , guesses :: [String]
  , matches :: [String]
  , maxGuesses :: Int
  , words :: [String]
  , foundGrays :: [Char]
  , foundYellows :: LetterDistribution
  , foundGreens :: String
  , hasLiedAlready :: Bool
  , lieProbability :: Double
  } deriving (Show, Eq)

lastGuess :: GameState -> String
lastGuess state = head $ guesses state

lastMatch :: GameState -> String
lastMatch state = head $ matches state

currentGuesses :: GameState -> Int
currentGuesses state = length $ guesses state