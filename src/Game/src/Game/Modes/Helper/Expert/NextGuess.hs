module Game.Modes.Helper.Expert.NextGuess (filterSet, makeNextGuess, filterDictionaries, flatten) where

import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.List (sortBy)

import Game.Modes.Helper.Standard.NextGuess (filterSet, calculateScoreForWords, argMaxBy, boolToInt)


makeNextGuess :: [String] -> [[String]] -> String
makeNextGuess possibleWords dictionaries =
  fst $ argMaxBy snd $ zip possibleWords $ calculateScoreForMultipleDictionaries possibleWords dictionaries

orderByScore :: [(String, Int)] -> [String] -> [(String, Int)]
orderByScore scoredWords currentGuesses =
  sortBy (\(word1, score1) (word2, score2) ->
    if score1 == score2
    then compare (boolToInt $ elem word1 currentGuesses)
          (boolToInt $ elem word2 currentGuesses)
    else compare score1 score2) scoredWords

calculateScoreForMultipleDictionaries :: [String] -> [[String]] -> [Int]
calculateScoreForMultipleDictionaries words [] = replicate (length words) 0
calculateScoreForMultipleDictionaries words (dictionary:ds) =
  addArray
    (calculateScoreForWords words words dictionary)
    (calculateScoreForMultipleDictionaries words ds)

addArray :: [Int] -> [Int] -> [Int]
addArray [] [] = []
addArray (x:xs) (y:ys) = (x + y) : (addArray xs ys)

filterDictionaries :: [[String]] -> String -> String -> [[String]]
filterDictionaries [] _ _ = []
filterDictionaries (dictionary:ds) guessWord matchWord =
  (filterSet guessWord matchWord dictionary)
  : (filterDictionaries ds guessWord matchWord)

flatten :: [[String]] -> [String]
flatten ll = Set.toList $ Set.fromList $ concat ll
