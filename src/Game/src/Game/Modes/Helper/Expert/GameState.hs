module Game.Modes.Helper.Expert.GameState (GameState(..), lastGuess, lastMatch) where

import Game.Modes.Helper.Difficulty (Difficulty(..))
import Game.Typing (MatchedGuesses)


data GameState = GameState
  { possibleWords :: [String]
  , guesses :: MatchedGuesses
  , lieDictionaries :: [[String]]
  , nonLieDictionary :: [String]
  , nextGuess :: String
  , currentGuesses :: Int
  , wordLength :: Int
  } deriving (Show, Eq)

lastGuess :: GameState -> String
lastGuess state = fst $ head $ guesses state

lastMatch :: GameState -> String
lastMatch state = snd $ head $ guesses state
