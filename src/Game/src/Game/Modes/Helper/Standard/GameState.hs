module Game.Modes.Helper.Standard.GameState (GameState(..), lastGuess, lastMatch) where

import Game.Modes.Helper.Difficulty (Difficulty(..))
import Game.Typing (MatchedGuesses)


data GameState = GameState
  { possibleWords :: [String]
  , guesses :: MatchedGuesses
  , nextGuess :: String
  , currentGuesses :: Int
  , wordLength :: Int
  } deriving (Show, Eq)

lastGuess :: GameState -> String
lastGuess state = fst $ head $ guesses state

lastMatch :: GameState -> String
lastMatch state = snd $ head $ guesses state
