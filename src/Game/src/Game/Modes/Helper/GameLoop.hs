module Game.Modes.Helper.GameLoop (helperGameLoop, Difficulty(..)) where

import Game.Modes.Helper.Standard.GameLoop (helperStandardGameLoop)
import Game.Modes.Helper.Expert.GameLoop (helperExpertGameLoop)
import Game.Modes.Helper.Difficulty (Difficulty(..))
import Database.Reader (readSmallWordSetWithLength)
import Game.Modes.Helper.ConsoleMode (printLine)
import Game.Console.ColorPrinter (colorizeTextFromEnumColor, Color(..))


helperGameLoop :: Int -> Difficulty -> IO Bool
helperGameLoop wordLength difficulty = do
  wordsDB <- readSmallWordSetWithLength wordLength
  printLine $
    (colorizeTextFromEnumColor Magenta "Since this is a more challenging task, you need to chose the word from the limited set: ")
    ++ (colorizeTextFromEnumColor Yellow $ show wordsDB)

  case difficulty of
    Standard -> helperStandardGameLoop wordLength
    Expert -> helperExpertGameLoop wordLength