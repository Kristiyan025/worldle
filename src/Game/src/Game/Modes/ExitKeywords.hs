module Game.Modes.ExitKeywords (exitWord, exitMessage) where

exitWord :: String
exitWord = "quit"

exitMessage :: String
exitMessage = "Aborting..."
