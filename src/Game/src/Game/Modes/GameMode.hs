module Game.Modes.GameMode (GameMode(..), Difficulty(..), readGameMode, readDifficulty) where

data GameMode = Play | Helper | NoMode deriving (Show, Eq)

data Difficulty = Easy | Standard | Expert | NoDifficulty deriving (Show, Eq)

readDifficulty :: String -> Difficulty
readDifficulty "easy" = Easy
readDifficulty "standard" = Standard
readDifficulty "expert" = Expert
readDifficulty _ = NoDifficulty

readGameMode :: String -> GameMode
readGameMode "play" = Play
readGameMode "helper" = Helper
readGameMode _ = NoMode
