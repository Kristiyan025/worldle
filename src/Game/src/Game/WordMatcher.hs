module Game.WordMatcher
  (matchWord
  , makeLetterDistribution
  , addLetterToDistribution
  , getLetterCount
  , LetterDistribution
  , doesWordObeyMatching) where

import qualified Data.Map as Map

import Game.GameColors (gray, yellow, green, invalid)

type LetterDistribution = Map.Map Char Int


matchWord :: String -> String -> String
matchWord guessWord secretWord = let
  secretWordLetterDistribution = makeLetterDistribution secretWord
  (partialMatch, currentGuessWordLetterDistribution) = matchFirstGreens guessWord secretWord
      Map.empty secretWordLetterDistribution :: (String, LetterDistribution)
  fullMatch = matchWorYellowAndGrey guessWord secretWord currentGuessWordLetterDistribution
      secretWordLetterDistribution partialMatch :: String
  in fullMatch

doesWordObeyMatching :: String -> String -> String -> Bool
doesWordObeyMatching word match guessWord = (matchWord guessWord word) == match

matchFirstGreens :: String -> String -> LetterDistribution -> LetterDistribution -> (String, LetterDistribution)
matchFirstGreens [] [] currentGuessWordLetterDistribution _ = ([], currentGuessWordLetterDistribution)
matchFirstGreens [] _ _ _ = error "Secret word is longer."
matchFirstGreens _ [] _ _ = error "Guess word is longer."
matchFirstGreens
  (guessFirstLetter:gs)
  (secretFirstLetter:ss)
  currentGuessWordLetterDistribution
  secretWordLetterDistribution = let
    newGuessWordLetterDistribution = addLetterToDistribution guessFirstLetter
      currentGuessWordLetterDistribution :: LetterDistribution
    in if guessFirstLetter == secretFirstLetter
      then ([green] ++ (fst $ matchFirstGreens gs ss newGuessWordLetterDistribution secretWordLetterDistribution)
        , newGuessWordLetterDistribution)
      else ([invalid] ++ (fst $ matchFirstGreens gs ss currentGuessWordLetterDistribution secretWordLetterDistribution)
        , currentGuessWordLetterDistribution)

matchWorYellowAndGrey :: String -> String -> LetterDistribution -> LetterDistribution -> String -> String
matchWorYellowAndGrey [] [] _ _ _ = []
matchWorYellowAndGrey [] _ _ _ _ = error "Secret word is longer."
matchWorYellowAndGrey _ [] _ _ _ = error "Guess word is longer."
matchWorYellowAndGrey
  (guessFirstLetter:gs)
  (secretFirstLetter:ss)
  currentGuessWordLetterDistribution
  secretWordLetterDistribution
  (partialMatchFirstSymbol:ps) = let
    newGuessWordLetterDistribution = addLetterToDistribution guessFirstLetter
      currentGuessWordLetterDistribution :: LetterDistribution
    restOfGuessWordMatch = matchWorYellowAndGrey gs ss newGuessWordLetterDistribution
      secretWordLetterDistribution ps :: String
    in if guessFirstLetter == secretFirstLetter
        then [partialMatchFirstSymbol] ++ restOfGuessWordMatch
    else if (getLetterCount guessFirstLetter currentGuessWordLetterDistribution)
          < (getLetterCount guessFirstLetter secretWordLetterDistribution)
        then [yellow] ++ restOfGuessWordMatch
        else [gray] ++ restOfGuessWordMatch

makeLetterDistribution :: String -> LetterDistribution
makeLetterDistribution [] = Map.empty
makeLetterDistribution (x:xs) = let
    letterDistribution = makeLetterDistribution xs :: LetterDistribution
    in addLetterToDistribution x letterDistribution

addLetterToDistribution :: Char -> LetterDistribution -> LetterDistribution
addLetterToDistribution letter letterDistribution = case Map.lookup letter letterDistribution of
    Just value -> Map.insert letter (value + 1) letterDistribution
    Nothing -> Map.insert letter 1 letterDistribution

getLetterCount :: Char -> LetterDistribution -> Int
getLetterCount letter letterDistribution = case Map.lookup letter letterDistribution of
    Just value -> value
    Nothing -> 0
