module Database.FileSystem.ContentReader (readContents) where

readContents :: FilePath -> IO [String]
readContents filePath = do
  -- Read the content of the file
  fileContent <- readFile filePath

  -- Get the lines of the file
  let fileLines = lines fileContent

  return fileLines
