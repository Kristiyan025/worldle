module Database.Reader(readMainWordSetWithLength, readSmallWordSetWithLength, minimumLength, maximumLength) where

import Text.Printf (printf)
import System.Directory (doesFileExist)
import Data.Time.Clock (getCurrentTime, diffUTCTime, UTCTime, NominalDiffTime)

import Database.FileSystem.DBFilepath (getMainDbFilePath, getSmallDbFilePath)
import Database.FileSystem.ContentReader (readContents)
import Database.Hydration.DBShardCreator (createMainDBFile, createSmallDBFile, calculateElapsedTime)
import Database.Hydration.WordPagesFlattener (minimumLength, maximumLength)


readMainWordSetWithLength :: Int -> IO [String]
readMainWordSetWithLength wordLength = do
  readWords wordLength getMainDbFilePath createMainDBFile

readSmallWordSetWithLength :: Int -> IO [String]
readSmallWordSetWithLength wordLength = do
  readWords wordLength getSmallDbFilePath createSmallDBFile

readWords :: Int -> (Int  -> IO String) -> (Int -> Bool -> IO ()) -> IO [String]
readWords wordLength getFilepath hydrateDB = do
    filePath <- getFilepath wordLength
    fileExists <- doesFileExist filePath
    _ <- hydrateDatabaseFile $ not fileExists
    readContents filePath
    where
    hydrateDatabaseFile :: Bool -> IO ()
    hydrateDatabaseFile False = return ()
    hydrateDatabaseFile True = do
      putStrLn $ "Database doesn't contain words with length " ++ (show wordLength)
              ++ ". Hydrating database (could take a while)..."
      start <- getCurrentTime
      _ <- hydrateDB wordLength False
      end <- getCurrentTime
      let elapsedTime = calculateElapsedTime start end :: Double
      putStrLn $ "Finished hydrating database with words with length " ++ (show wordLength) ++
        ". Took " ++ (printf "%.2f" elapsedTime) ++ " seconds"