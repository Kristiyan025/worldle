module Database.Hydration.DataFetcher (requestPage) where

import System.Process.Typed
import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.Text.Encoding as TE
import Data.Text (Text, unpack)


textToString :: Text -> String
textToString = unpack

createCURL :: Int -> Int -> String
createCURL wordLength page =
  "curl -s --location 'https://wordfinder.yourdictionary.com/letter-words/" ++
  (show wordLength) ++ "/?dictionary=WWF&page=" ++ (show page) ++ "'"

requestPage :: Int -> Int -> IO String
requestPage wordLength page = do
  let cURL = createCURL wordLength page
  -- Run the curl command
  (_, out, _) <- readProcess $ setStdin closed $ shell $ cURL
  -- Convert lazy bytestring to strict text
  let strictTextOutput = TE.decodeUtf8 $ L8.toStrict out

  return (textToString strictTextOutput)
