module Database.Hydration.WordPagesFlattener (fetchWordsWithLength, wordLengths, minimumLength, maximumLength) where

import qualified Data.Map as Map

import Database.Hydration.DataFetcher (requestPage)
import Database.Hydration.WordExtractor (extractWords)


wordLengthToPageCountMap :: Map.Map Int Int
wordLengthToPageCountMap = Map.fromList [
  (4, 46),
  (5, 101),
  (6, 176),
  (7, 262),
  (8, 322),
  (9, 298),
  (10, 253)]

wordLengths :: [Int]
wordLengths = Map.keys wordLengthToPageCountMap

minimumLength :: String
minimumLength = show $ minimum wordLengths

maximumLength :: String
maximumLength = show $ maximum wordLengths

fetchPageWords :: Int -> Int -> IO [String]
fetchPageWords wordLength page = do
  response <- requestPage wordLength page
  let words = extractWords response :: [String]
  return words

fetchWordsWithLength :: Int -> IO [String]
fetchWordsWithLength wordLength = do
  case Map.lookup wordLength wordLengthToPageCountMap of
    Just value -> do
      let pages = [1..value] :: [Int]
      words <- mapM (fetchPageWords wordLength) pages
      let flattenedWords = concat words :: [String]
      return flattenedWords
    Nothing    -> error $ "Cannot fetch words with length " ++ show wordLength ++ ", must be in the interval [" ++
      minimumLength ++ ", " ++ maximumLength ++ "]"
