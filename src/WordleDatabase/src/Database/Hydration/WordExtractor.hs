module Database.Hydration.WordExtractor (extractWords) where

import Text.Regex.Posix
import Text.Regex.Posix.Wrap
import Data.Char (isAscii)

wordTagRegex :: String
wordTagRegex = "<td data-testid=\"word_results_table_row\" \
                              \class=\"table__cell table__cell--first\" data-v-e1a24138>([a-z]+)</td>"

getMatches :: String -> String -> [String]
getMatches input regex = do
  let rawMatches = input =~ regex :: AllTextMatches [] String
  let matches = map id $ getAllTextMatches rawMatches
  matches


extractWordFromMatch :: String -> String -> String
extractWordFromMatch regex match = do
  let (_, _, _, groups) = match =~ regex :: (String, String, String, [String])
  case groups of
    [] -> ""
    (x:_) -> x

extractWordsWithRegex :: String -> String -> [String]
extractWordsWithRegex input regex = do
  let matches = getMatches input regex
  let words = map (extractWordFromMatch regex) matches
  words

cleanUpString :: String -> String
cleanUpString = filter isAscii


extractWords :: String -> [String]
extractWords input = extractWordsWithRegex (cleanUpString input) wordTagRegex