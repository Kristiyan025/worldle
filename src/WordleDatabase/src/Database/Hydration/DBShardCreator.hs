module Database.Hydration.DBShardCreator (createMainDBFile, createSmallDBFile, calculateElapsedTime) where

import Text.Printf (printf)
import System.Directory (getCurrentDirectory, doesFileExist, createDirectoryIfMissing)
import System.FilePath ((</>), takeFileName, takeDirectory)
import Data.Text.Lazy (Text)
import Data.Text.Lazy (Text, intercalate, pack)
import Data.Text.Lazy.IO (writeFile)
import Data.Time.Clock (getCurrentTime, diffUTCTime, UTCTime, NominalDiffTime)
import System.Random (randomRIO)

import Database.FileSystem.DBFilepath (getMainDbFilePath, getSmallDbFilePath)
import Database.Hydration.WordPagesFlattener (fetchWordsWithLength)
import Database.FileSystem.ContentReader (readContents)


lineSeparator = pack "\n" :: Text

smallDbSetSize = 100 :: Int

createMainDBFile :: Int -> Bool -> IO ()
createMainDBFile wordLength printDebugInfo = do
  hydrateDBFile wordLength printDebugInfo getMainDbFilePath fetchWordsWithLength

createSmallDBFile :: Int -> Bool -> IO ()
createSmallDBFile wordLength printDebugInfo = do
  hydrateDBFile wordLength printDebugInfo getSmallDbFilePath fetchWords
  where
    fetchWords :: Int -> IO [String]
    fetchWords wordLength = do
      mainDbFilePath <- getMainDbFilePath wordLength
      _ <- createMainDBFile wordLength False
      words <- readContents mainDbFilePath
      sample smallDbSetSize words

hydrateDBFile :: Int -> Bool -> (Int -> IO FilePath) -> (Int -> IO [String]) -> IO ()
hydrateDBFile wordLength printDebugInfo getFilePath fetchWords = do
  start <- getCurrentTime
  dbFilePath <- getFilePath wordLength
  let dbFileName = takeFileName dbFilePath :: String

  fileExists <- doesFileExist dbFilePath
  if fileExists
  then do
    if printDebugInfo
    then putStrLn $ "Database file already exists: " ++ dbFileName
    else return ()
  else do
    _ <- createDirectoryIfMissing True $ takeDirectory dbFilePath

    words <- fetchWords wordLength

    _ <- Data.Text.Lazy.IO.writeFile dbFilePath $ formatDbFileContents words

    end <- getCurrentTime
    let elapsedTime = calculateElapsedTime start end :: Double
    if printDebugInfo
    then do
      putStrLn $ "Finished creating database file: " ++ dbFileName ++
        " in " ++ (printf "%.2f" elapsedTime) ++ " seconds"
    else return ()

calculateElapsedTime :: UTCTime -> UTCTime -> Double
calculateElapsedTime start end = realToFrac $ diffUTCTime end start

formatDbFileContents :: [String] -> Text
formatDbFileContents words = intercalate lineSeparator (map pack words)

-- Had problems of installing random-extras, because of the dependency on random-source
-- and random-source depends on base > 4 && < 4.16, and cabal couldnt resolve the dependencies,
-- so I just implementated 'sample' myself.
sample :: Int -> [a] -> IO [a]
sample n xs = do
  randomIds <- randomIndices n $ length xs
  mapM (\index -> return $ xs !! index) randomIds
  where
    randomIndices :: Int -> Int -> IO [Int]
    randomIndices n listLength =
      if n > listLength
      then error $ "The list is smaller from the given sample size. {n="
        ++ (show n) ++ ", listLength=" ++ (show listLength) ++ "}"
      else randomIndicesHelper n listLength []

    randomIndicesHelper :: Int -> Int -> [Int] -> IO [Int]
    randomIndicesHelper 0 _ currentIndices = return currentIndices
    randomIndicesHelper n listLength currentIndices = do
      randomIndex <- randomRIO (0, listLength - 1)

      if randomIndex `elem` currentIndices
        then randomIndicesHelper n listLength currentIndices
        else randomIndicesHelper (n - 1) listLength (randomIndex:currentIndices)
