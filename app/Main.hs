module Main where

import Game.GameLoop (mainGameLoop)


main :: IO ()
main = do
  _ <- mainGameLoop ()
  return ()
