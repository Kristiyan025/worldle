# Wordle

[Repository](#blank)

<details open>
<summary><b style="font-size: 4vh">Contents</b></summary>

* [Snapshots](#snapshots)
* [Project Usage](#project-usage)
  * [Requirements](#requirements)
  * [How to build the project](#how-to-build-the-project)
  * [How to run the project](#how-to-run-the-project)
  * [How to hydrate the database](#how-to-hydrate-the-database)
  * [How to Play](#how-to-play)
* [Development](#development)
    * [Project Structure](#project-structure)
    * [Database Schema](#database-schema)
    * [Hydrating the database](#hydrating-the-database)
    * [Game modes](#game-modes)
    * [CLI](#cli)
    * [Main Functions](#main-functions)
    * [Development Process & Issues](#development-process--issues)
        * [Development Chronological Order](#development-chronological-order)
        * [Development Issues](#development-issues)
</details>

## Snapshots
![Preview 1](./static/preview%201.jpeg)
![Preview 2](./static/preview%202.jpeg)

<style type="text/css">
    img {
        height: 500px;
    }
</style>

## Project Usage
### Requirements
To run the project, you need to have installed:
* The Cabal package manager
* The GHC compiler

You can install both as described in the
[Installation guide](https://cabal.readthedocs.io/en/latest/getting-started.html#installing-cabal).

### How to build the project
Inside the project root directory, run the following commands:

    $ cabal update
    $ cabal build --verbose=0

### How to run the project
Inside the project root directory, run the following commands:

    $ cabal run Wordle

### How to hydrate the database
For simplicity, we've hydrated already the database with the word.
included it in the project, since its size is small (~1,5MB).

But if you want to hydrate the database yourself, you can do it.

> **_WARING:_** If you want to hydrate the database yourself, 
you can do it, but even though the database is small, it takes 
a long time to hydrate it (~10-20 minutes). 

You can do it with the following command:

    $ cabal run DatabaseHydrator

and if you want to hydrate the database with printing debug info, 
you can use the following command:

    $ cabal run DatabaseHydrator print-debug-info=True

### How to Play
* The game CLI is rather intuitive, and tells the user what to do.
* The only thing not described in the CLI is the quitting:
  * To quit a game mode, type `quit`.
  * To quit the game (when not in a game mode), type `exit`.

## Development
### Project Structure
The project consists of the following libraries:
* `Database` - The database library. It's responsible for:
  * Hydrating the database files.
  * Reading from the database files.
* `Game` - The game library. It's responsible for:
  * The game logic.
  * The game modes.
  * The game CLI.

The project consists of the following executables:
* `DatabaseHydrator` - The executable for hydrating the database.
* `DBShardProcessMain` - Internal executable for hydrating the database.
  * It's used by `DatabaseHydrator`.
  * It's ran in a separate process for each word length.
* `Wordle` - The main executable for playing the game.

The files are organized in the following way:
* `src` - All source files.
* Each library has its onw directory in `src`.
* Each library has its `src` directory & `*.cabal` configuration file.
* The `src` directory of each library contains 1 directory 
  with the same name as the library. 

This file structure is weird, but used because:
* I had a hard time configuring multiple libraries in a single project.
* The cabal files cannot stay i the same directory.
* I wanted the source file to be separated from the cabal files.

### Database Schema
* The words for each length are stored in a separate file.
* Since the algorithm for the `Helper` mode is not so efficient,
  We need to use a smaller database for it. That's why we have
  a separate database files for this mode containing 100 words.

### Hydrating the database
I didn't want to use a ready file from the internet,
because I couldn't find a big one.

Despite that, I found a website that has a lot of words,
and I decided to use it to hydrate the database.

Alas, the website doesn't have an API, so I had to scrape it.
I used the following website:

> https://wordfinder.yourdictionary.com/letter-words/{word-length}/?dictionary=WWF&page={page}

I couldn't find a REST request that returns all the words only,
so I do the following:
1. I send a HTTPS request to the website with the following parameters:
   1. `word-length`
   2. `page` - The website paginates the results
      only a small number of words per page.
   - The number of pages for each word length cannot be fetched via REST,
   so that information is hardcoded in the code.
> So I have to sent for each word length a request for each page
  (if I want to get all the words, which I do).

2. I get the response, and I parse it. That includes:
   * Regex matching the HTML, i.e. the specific HTML tags 
     that contain the words.
   * Group extracting.
      - I couldn't both match all tags via regex (probably I couldn't 
        find the right function/parameters in Haskell), and extract the words
        from them, so I had to do it in two steps.
      1. Extract all the tags.
      2. Extract the words from each tag separately.
3. I insert the words in the database.
   * As simple as writing to a file.
4. Since that process takes a lot of time, I parallelized it.
   * I spawn a new process for each word length.

### Game modes
The game has 2 game modes:
1. `Play` - The user plays the game.
   - The user has to guess the word.
   - The computer tells the user how "close" he is to the word.
   - If the user guesses the word, the computer prints green 
     for all letters in the word & game ends.
   - The mode has 3 difficulties:
     1. `Easy` - The computer warns the user if:
        * The user tries to guess a word that is not in the database.
        * The user doesn't use green letters on their correct 
          positions & or at all.
        * The user uses letters that are already marked as gray.
        * Te user doesn't use letters that are marked as yellow.
     2. `Standard`
        * For each guess, the computer tells the user how is 
          that word "matched" to the secret word.
     3. `Hard`
        * The computer is allowed to lie to the user once,
          but the lie match should not be contradicting with past answers.
   - The user's allowed number of guesses is based on the word length & 
     difficulty of the mode.
2. `Helper` - The computer plays the game.
    - The computer has to guess the word.
    - The user tells the computer how "close" it is to the word.
    - The game ends when the user gives green for all letters in the word.
    - The mode has 2 difficulties:
      2. `Standard`
          * For each guess, the user tells the computer how is 
             that word "matched" to the secret word.
      3. `Hard`
          * The user is allowed to lie to the computer once.

Each mode is played in a REPL (Read-Eval-Print-Loop) manner.
  * The user gives input, and the computer responds.
  * The user can exit the game at any time by typing `exit`.

### CLI
The whole game is played in the CLI which is simulated once again 
in a REPL manner.

The CLI is simulated with a main console, i.e. each line begines with ` > `.
Each game mode is simulated in a sub-console, i.e. each line begins with `~~> `.

To exit:
* From the main console, type `exit`.
* From a sub-console, type `quit`.

Since the CLI looks rather simple/ugly, I've decided to colorize it.

### Main Functions
* `Database.Reader(readMainWordSetWithLength, readSmallWordSetWithLength)`
  * The database readers for the corresponding database files 
    for the different modes.
* `Database.Hydrator (hydrateDatabase)`
  * Hydrate the database with the words from the website.
* `Database.Hydration.DataFetcher (requestPage)`
  * Fetch the HTML page from the website for specific pair (word-length, page).
* `Database.Hydration.WordExtractor (extractWords)`
  * Extract the words from the HTML page.
* `Database.Hydration.WordPagesFlattener (fetchWordsWithLength)`
  * Fetch all the words for a specific word length.
* `Database.Hydration.DBShardCreator (createMainDBFile, createSmallDBFile)`
  * Create the database files for the different modes.
* `DBShardProcessMain.hs>Main (main)`
  * The main function for each database hydration process.
* `Game.Console.GameLoop (gameLoop)` - The abstract game loop.
  It's responsible for:
    * Initializing the game.
    * Until the game ends:
      * Reads input from the user (if needed).
      * Updates the game state.
      * Responds to the user (if needed).
* `Game.GameLoop (mainGameLoop)`
  * The main game loop. Effectively, the main function of the game.
* `Game.Modes.Play.GameLoop (playGameLoop)`
  * The game loop for the `Play` mode.
* `Game.Modes.Helper.GameLoop (helperGameLoop)`
  * The game loop for the `Helper` mode.
* `Game.Console.ColorPrinter (colorizeText, colorizeTextFromEnumColor)`
  * The utility functions for colorizing the CLI.
* `Game.WordMatcher (matchWord)`
  * Given guess & secret word, generate the match for them.

### Development Process & Issues
#### Development Chronological Order
1. Hydrated the database.
3. Created the abstract game loop.
4. Implemented the `Play` mode.
5. Implemented the `Helper` mode.
6. Implemented the main CLI mode.

#### Development Issues
* I struggled a lot wih finding the right way to send & await the HTTPS request.
  * Finally, I settled on just starting a process hitting a cURL.
* I struggled a lot with finding how to extract a group from 
  all regexes with 1 function.
  * Finally, I settled on first finding all matches, and then 
    extracting the group from each match.
* I struggled a lot with finding the righ way to parallelize the 
  database hydration.
  * I wanted to spawn new processes for each word length, and 
    Each one of them to use multiple threads.
  * Although the code ran, the timing benchmarks showed that 
    the code was slower this way.
  * Finally, I settled on using a multiple processes, with a single thread.
* I struggled a lot with setting up the cabal configuration files,
  when the project became more complex.
* I took my time about thinking on how I should implement the 
  bonus for the `Helper` mode.
